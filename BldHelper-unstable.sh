#!/bin/bash

# BldHelper-nightly.sh
# This script is meant to be run on the build server and expects to find and update itself from adjacent repos.
# From PepDistroConfigs, these repos are ../PepProPixMaps & ../PepProTools and are vital to having a working build.

### ## # Set build working variables HERE # ## ###

PREFIX=AcorOS-lxde		# Sets a unique final name of the ISO and checksum so <HouseKeeping> only removes 2 files .
SUFFIX=i386		# Also used by <HouseKeeping>. And to distinguish between amd64 and x86 or devuan and ubuntu .
BUILD=unstable-builds		# Sets which pepbld.sh to use and the location in /var/www/html/[release|rc|testing|nightly|unstable]

##################################################
### ## # Make NO Edits Below This Line !! # ## ###
##################################################

TODAY=$(date -u +"%Y-%m-%d") && export TODAY		# If MasterBuilder.sh is used IT will set the date. If not used, we set it here.
FileName=${PREFIX}-${SUFFIX}-${TODAY}-${BUILD}		# This will give a uniquely named and dated ISO and checksum for <HouseKeeping>.
LOCATION="/var/www/acoroslinux.tk/${BUILD}"			# Tells <HouseKeeping> and the script which 2 files to remove and where to put them.
LogDir="/var/log/Live-Build"				# This folder contains a log for the last $[PREFIX]-$[SUFFIX] build.
WorkingDir=~/builder/Lxqt86_32			# * If we change servers or locations T*H*I*S line is the O*N*L*Y line to change. *
OutFile="/tmp/${PREFIX}${SUFFIX}.out"
LogFile="${LogDir}/${PREFIX}-${SUFFIX}-${BUILD}.log"
_cache="./cache"
_break=0 ; _wait=30	# Time (in seconds) to wait

cd ${WorkingDir} 
./RepoUpdater | tee ${OutFile}

# Run the build script - expect 50 minutes, allow 60.
./unstablebuild.sh 2>&1 | tee --append ${OutFile}

# Timing matters, don't destroy the old one without a replacement.
# Check for the ISO to appear and wait for things to settle.
until [ -e build/*.iso ]
      do ((++_break))
	 [ $_break -gt $_wait ] && break || sleep 1
done

if [ ${_break} -lt ${_wait} ] ; then

mv build/*.iso build/${FileName}.iso

# Define a 2GiB partition, at offset 4194304, in the ISO's 3rd entry of the MBR .
#dd bs=1 count=16 seek=478 conv=notrunc if=peploadersplash/P4-2GB-MBR.hex of=fusato/${FileName}.iso

# Make the checksum file.
cd build
echo "# ${FileName}" > ${FileName}-sha512.checksum
sha512sum ${FileName}.iso >> ${FileName}-sha512.checksum

### <HouseKeeping>
# Remove the previous files in ${LOCATION} .
rm -f ${LOCATION}/${PREFIX}-${SUFFIX}*.iso
rm -f ${LOCATION}/${PREFIX}-${SUFFIX}*-sha512.checksum
#rm -f ${LOCATION}/${PREFIX}-${SUFFIX}*.torrent

# This is where we can create the .torrent file.

#mv $(FileName}*  ${LOCATION}/
mv ${FileName}.iso             ${LOCATION}/${FileName}.iso
mv ${FileName}-sha512.checksum ${LOCATION}/${FileName}-sha512.checksum
#mv ${FileName}.torrent         ${LOCATION}/${FileName}.torrent

# touch -t ${_stamp} ${LOCATION} ${LOCATION}/${FileName}*
touch ${LOCATION}/${FileName}*

# Update netinstall-packages.yaml
#diff chroot/etc/calamares/modules/netinstall-packages.yaml ../pepcal/calamares/modules/netinstall-packages.yaml || (
#  cp chroot/etc/calamares/modules/netinstall-packages.yaml ../pepcal/calamares/modules/ 
#  git add ../pepcal/calamares/modules/netinstall-packages.yaml
#  git commit -m "Automated update to file, after build completed." ; git push )

lb clean &

# Move the log file to the log directory.
[ ! -e ${LogDir} ] && mkdir -p ${LogDir}
mv ${OutFile} ${LogFile}

# Remove old packages from the cache directory
for i in $(grep "Del " ${LogFile} | sort -u | cut -f2,3 -d" " | tr " " "_" | tr ":" "*" | tr "+" "*" )
           do for j in $_cache/packages.*/${i}*.deb
                  do  [ -e $j ] && rm $j
              done
done

### </HouseKeeping>

else echo -e "\n\tAfter $_break seconds, ISO never appeared.\n" | tee --append ${OutFile}
     mv ${OutFile} ${LogFile}
fi

