#!/bin/bash

##BldHelper-testing.sh
### ## # Set build working variables HERE # ## ###

PREFIX=AcorOS-lxde		# Sets a unique final name of the ISO and checksum so <HouseKeeping> only removes 2 files .
SUFFIX=i386		# Also used by <HouseKeeping>. And to distinguish between amd64 and x86 or devuan and ubuntu .
BUILD=testing-builds		# Sets which pepbld.sh to use and the location in /var/www/html/[release|rc|testing|nightly|unstable]

##################################################
### ## # Make NO Edits Below This Line !! # ## ###
##################################################

[ $TODAY ! = " " ] || TODAY=$(date -u +"%Y-%m-%d")	# If MasterBuilder.sh is used IT will set the date. If not used, we set it here.
FileName=${PREFIX}-${SUFFIX}-${TODAY}-${BUILD}		# This will give a uniquely named and dated ISO and checksum for <HouseKeeping>.
LOCATION=/var/www/acoroslinux.tk/${BUILD}				# Tells <HouseKeeping> and the script which 2 files to remove and where to put them.
LogDir=/var/log/Live-Build				# This folder contains a log for the last $[PREFIX]-$[SUFFIX] build.
#WorkingDir=~/pep_builder/${PREFIX}${SUFFIX}		# * If we change servers or locations T*H*I*S line is the O*N*L*Y line to change. *
WorkingDir=~/builder/Lxqt86_32			# * If we change servers or locations T*H*I*S line is the O*N*L*Y line to change. *
cd ${WorkingDir} 
./RepoUpdater | tee ${OutFile}

# Run the build script - expect 50 minutes, allow 60.
./testbuild.sh 2>&1 | tee --append /tmp/${PREFIX}${SUFFIX}.out

# Timing matters, don't destroy the old one without a replacement.
# Check for the ISO to appear and wait for things to settle.
until [ -e build/*.iso ]
      do sleep 2 # Waiting for ISO image
done

cd build
mv *.iso ../${FileName}.iso
lb clean &
cd ../

# Define a 2GiB partition, at offset 4194304, in the ISO's 4th entry of the MBR .
#dd bs=1 count=16 seek=494 conv=notrunc if=peploadersplash/P4-2GB-MBR.hex of=${FileName}.iso

# Make the checksum file.
sha512sum ${FileName}.iso > ${FileName}-sha512.checksum


### <HouseKeeping>
# Remove the previous files in ${LOCATION} .
rm -f ${LOCATION}/${PREFIX}-${SUFFIX}*.iso
rm -f ${LOCATION}/${PREFIX}-${SUFFIX}*-sha512.checksum

# Move the log file to the log directory.
[ ! -e ${LogDir} ] && mkdir -p ${LogDir}
mv /tmp/${PREFIX}${SUFFIX}.out ${LogDir}/${PREFIX}-${SUFFIX}-${BUILD}.log

# Set the timestamps to the ISO's internal before moving them to the DL directory.
# _stamp=$(hexdump -C -s 33598 -n 12 ${FileName}.iso | head -1 | cut -f2 -d"|" )

#mv $(FileName}*  ${LOCATION}/
mv ${FileName}.iso             ${LOCATION}/${FileName}.iso
mv ${FileName}-sha512.checksum ${LOCATION}/${FileName}-sha512.checksum

# touch -t ${_stamp} ${LOCATION} ${LOCATION}/${FileName}*
touch ${LOCATION}/${FileName}*
### </HouseKeeping>


# This will "flush" our variables without handing any back to MasterBuilder.sh .
# exit	# But NOT `return`.

